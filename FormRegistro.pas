unit FormRegistro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Data.DB, Datasnap.DBClient, Data.FMTBcd, Data.SqlExpr;

type
  TfrmRegistro = class(TForm)
    Panel1: TPanel;
    BitBtnNovo: TBitBtn;
    Panel2: TPanel;
    EditId: TEdit;
    labelId: TLabel;
    EditNome: TEdit;
    LabelNome: TLabel;
    EditDescricao: TEdit;
    LabelDescricao: TLabel;
    EditPreco: TEdit;
    LabelPreco: TLabel;
    EditLucro: TEdit;
    LabelLucro: TLabel;
    EditPrecoFinal: TEdit;
    LabelPrecoFinal: TLabel;
    EditMarca: TEdit;
    Label1: TLabel;
    EditCategoria: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ComboBoxOrigem: TComboBox;
    EditCodigoDeBarras: TEdit;
    Label5: TLabel;
    procedure BitBtnNovoClick(Sender: TObject);
    procedure makeShowModal(cdsProdutos: TClientDataSet; FbConnection: TSQLConnection);
    procedure EditPrecoKeyPress(Sender: TObject; var Key: Char);
    procedure CalcularPreco();
    procedure FormatarComoMoeda( Componente : TObject; var Key: Char );
    procedure FormShow(Sender: TObject);
    procedure EditLucroEnter(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    pCdsProdutos: TClientDataSet;
    pFbConnection: TSQLConnection;
  public
    { Public declarations }
  end;


implementation

{$R *.dfm}

procedure TfrmRegistro.BitBtnNovoClick(Sender: TObject);
var
  vQry: TSQLQuery;
  vId: Integer;
begin
  try
    try
      if (pCdsProdutos.State in [dsinsert]) then
      begin
        vQry := TSQLQuery.Create(nil);
        with vQry do
        begin
          SQLConnection := pFbConnection;
          SQL.Clear;
          SQL.Add('Select max(ID) as ID');
          SQL.Add(' from produto p');
          Open;
          vId := vQry.FieldByName('ID').AsInteger + 1;
          pCdsProdutos.FieldByName('id').Value := vId;
        end;
      end;
      pCdsProdutos.FieldByName('nome').Value := EditNome.Text;
      pCdsProdutos.FieldByName('descricao').Value := EditDescricao.Text;
      pCdsProdutos.FieldByName('preco').Value := Trim(StringReplace(EditPreco.Text, 'R$ ','',[rfReplaceAll]));
      pCdsProdutos.FieldByName('lucro').Value := Trim(StringReplace(EditLucro.Text, '%','',[rfReplaceAll]));
      pCdsProdutos.FieldByName('precofinal').Value := Trim(StringReplace(EditPrecoFinal.Text, 'R$ ','',[rfReplaceAll]));
      pCdsProdutos.FieldByName('marca').Value := EditMarca.Text;
      pCdsProdutos.FieldByName('categoria').Value := EditCategoria.Text;
      pCdsProdutos.FieldByName('origem_id').Value := ComboBoxOrigem.ItemIndex;
      pCdsProdutos.FieldByName('codigobarras').Value := EditCodigoDeBarras.Text;
      pCdsProdutos.ApplyUpdates(-1);
      Close();
    finally
    FreeAndNil(vQry);
    end;
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmRegistro.BitBtnNovoClick : ' + E.Message);
    end;
  end;
end;

procedure TfrmRegistro.CalcularPreco;
var
  vPreco, vLucro: Double;
begin
  try
    if ((EditPreco.Text <> EmptyStr) and (EditLucro.Text <> EmptyStr)) then
    begin
      EditPreco.Text := Trim(StringReplace(EditPreco.Text, '.', '', [rfReplaceAll, rfIgnoreCase]));
      vPreco := StrToFloat(StringReplace(EditPreco.Text, 'R$ ','', [rfReplaceAll, rfIgnoreCase]));

      EditLucro.Text := Trim(StringReplace(EditLucro.Text, '.', '', [rfReplaceAll, rfIgnoreCase]));
      vLucro := StrToFloat(StringReplace(EditLucro.Text, '% ','', [rfReplaceAll, rfIgnoreCase]));
      vLucro := (vLucro / 100) ;
      
      if ((vPreco = 0) or (vLucro = 0)) then
      begin
        EditPrecoFinal.Text := '0,00';  
      end
      else
      begin
        EditPrecoFinal.Text := 'R$ ' + FormatFloat('###,###,###,##0.00', vPreco + (vPreco * vLucro));
      end;
    end;
  except
    on E: Exception do
    begin
      raise Exception.Create('TfrmRegistro.CalcularPreco : ' + E.Message);
    end;
  end;
end;

procedure TfrmRegistro.FormatarComoMoeda( Componente : TObject; var Key: Char );
var
   str_valor  : String;
   dbl_valor  : double;
begin
  try
    if (Componente is TEdit) then
    begin
      if ( Key in ['0'..'9', #8, #9] ) then
      begin
         str_valor := TEdit( Componente ).Text ;
         str_valor := StringReplace(str_valor, 'R$ ','', [rfReplaceAll, rfIgnoreCase]);
         str_valor := StringReplace(str_valor, '% ','', [rfReplaceAll, rfIgnoreCase]);
         if (str_valor = EmptyStr) then
         begin
           str_valor := '0,00' ;
         end;
         if (Key in ['0'..'9']) then 
         begin
           str_valor := Concat( str_valor, Key ) ;
         end;
         
         str_valor := Trim( StringReplace( str_valor, '.', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         str_valor := Trim( StringReplace( str_valor, ',', '', [rfReplaceAll, rfIgnoreCase] ) ) ;

         dbl_valor := StrToFloat( str_valor ) ;
         dbl_valor := ( dbl_valor / 100 ) ;

         TEdit( Componente ).SelStart := Length( TEdit( Componente ).Text );
         if (TEdit( Componente ).Tag <> 1) then
         begin
          TEdit( Componente ).Text := 'R$ ' + FormatFloat('###,###,###,##0.00', dbl_valor ) ;
         end
         else
         begin
          TEdit( Componente ).Text := '% ' + FormatFloat('###,###,###,##0.00', dbl_valor ) ;
         end;
      end;
      if not( Key in [#8, #9] ) then
      begin
        key := #0;
      end;
    end;
  except
    on E: Exception do
    begin
      raise Exception.Create('TfrmRegistro.FormatarComoMoeda : ' + E.Message);
    end;
  end;
end;

procedure TfrmRegistro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    pCdsProdutos.CancelUpdates();
  except
    on E: Exception do
    begin
      raise Exception.Create('TfrmRegistro.FormClose : ' + E.Message);
    end;
  end;
end;

procedure TfrmRegistro.FormShow(Sender: TObject);
begin
  try
    if (pCdsProdutos.State in [dsedit]) then
    begin
      EditId.Text := pCdsProdutos.FieldByName('id').Value;
      EditNome.Text := pCdsProdutos.FieldByName('nome').Value;
      EditDescricao.Text := pCdsProdutos.FieldByName('descricao').Value;
      EditPreco.Text := pCdsProdutos.FieldByName('preco').Value;
      EditLucro.Text := pCdsProdutos.FieldByName('lucro').Value;
      EditPrecoFinal.Text := pCdsProdutos.FieldByName('precofinal').Value;
      EditMarca.Text := pCdsProdutos.FieldByName('marca').Value;
      EditCategoria.Text := pCdsProdutos.FieldByName('categoria').Value;
      ComboBoxOrigem.ItemIndex := pCdsProdutos.FieldByName('origem_id').Value;
      EditCodigoDeBarras.Text := pCdsProdutos.FieldByName('codigobarras').Value;
    end;
  except
    On E: Exception do
    begin
      raise Exception.Create('Error Message');
    end;
  end;
end;

procedure TfrmRegistro.EditLucroEnter(Sender: TObject);
begin
  try
    TEdit(Sender).Text := '0,00';
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmRegistro.EditLucroEnter : ' + E.Message);
    end;
  end;
end;

procedure TfrmRegistro.EditPrecoKeyPress(Sender: TObject; var Key: Char);
begin
  try
    FormatarComoMoeda(Sender, Key);
    CalcularPreco();
  except
    On E:Exception do
    begin
      raise Exception.Create('TfrmRegistro.EditPrecoKeyPress : ' + E.Message);
    end;
  end;
end;

procedure TfrmRegistro.makeShowModal(cdsProdutos: TClientDataSet; FbConnection: TSQLConnection);
begin
  try
    pCdsProdutos := cdsProdutos;
    pFbConnection := FbConnection;
    ShowModal();
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmRegistro.makeShowModal : ' + E.Message);
    end;
  end;
end;

end.

