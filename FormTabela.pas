unit FormTabela;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Data.FMTBcd, Datasnap.DBClient, Datasnap.Provider, Data.SqlExpr,
  Vcl.StdCtrls, Vcl.Buttons, FormRegistro, Data.DBXFirebird, IWVCLBaseControl,
  IWBaseControl, IWBaseHTMLControl, IWControl, IWCompGrids, IWDBGrids;

type
  TfrmTabela = class(TForm)
    Panel1: TPanel;
    BitBtnNovo: TBitBtn;
    BitBtnEditar: TBitBtn;
    BitBtnExcluir: TBitBtn;
    FBConnection: TSQLConnection;
    DBGrid1: TDBGrid;
    SQLQueryProduto: TSQLQuery;
    dsProviderProduto: TDataSetProvider;
    cdsProdutos: TClientDataSet;
    dsProduto: TDataSource;
    cdsProdutosID: TIntegerField;
    cdsProdutosNOME: TStringField;
    cdsProdutosDESCRICAO: TStringField;
    cdsProdutosPRECO: TStringField;
    cdsProdutosLUCRO: TStringField;
    cdsProdutosPRECOFINAL: TStringField;
    cdsProdutosMARCA: TStringField;
    cdsProdutosCATEGORIA: TStringField;
    cdsProdutosORIGEM_ID: TIntegerField;
    cdsProdutosCODIGOBARRAS: TStringField;
    procedure BitBtnNovoClick(Sender: TObject);
    procedure BitBtnEditarClick(Sender: TObject);
    procedure dsProdutosDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure BitBtnExcluirClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    private procedure CriaFormRegistro();
    private procedure AtualizarBotoes();
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
    vQry: TSQLQuery;

implementation

{$R *.dfm}

uses FormApresentacao;

procedure TfrmTabela.AtualizarBotoes;
begin
  try
    BitBtnEditar.Enabled := cdsProdutos.RecordCount > 0;
    BitBtnExcluir.Enabled := cdsProdutos.RecordCount > 0;
  except
    on E: Exception do
    begin
      raise Exception.Create('TfrmTabela.AtualizarBotoes : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.BitBtnEditarClick(Sender: TObject);
begin
  try
    cdsProdutos.Edit();
    CriaFormRegistro();
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmTabela.BitBtnEditarClick : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.BitBtnExcluirClick(Sender: TObject);
begin
  try
    if (Application.MessageBox('Deseja excluir este produto?', 'Excluir', MB_ICONQUESTION + MB_YESNO) = mrYes) then
    begin
      cdsProdutos.Delete();
      cdsProdutos.ApplyUpdates(-1);
    end;
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmTabela.BitBtnExcluirClick : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.BitBtnNovoClick(Sender: TObject);
begin
  try
    cdsProdutos.Append();
    CriaFormRegistro();
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmTabela.BitBtnNovoClick : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.CriaFormRegistro;
var
  frmRegistro: TfrmRegistro;
begin
  try
    Application.CreateForm(TfrmRegistro, frmRegistro);
    try
      frmRegistro.makeShowModal(cdsProdutos, FBConnection);
    finally
      FreeAndNil(frmRegistro);
    end;
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmTabela.CriaFormRegistro : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.dsProdutosDataChange(Sender: TObject; Field: TField);
begin
  try
    AtualizarBotoes();
  except
    on E: Exception do
    begin
      raise Exception.Create('TfrmTabela.dsProdutosDataChange : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    Application.Terminate();
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmTabela.FormClose : ' + E.Message);
    end;
  end;
end;

procedure TfrmTabela.FormShow(Sender: TObject);
var
  frmApresentacao: TfrmApresentacao;

begin
  try
    FBConnection.Params.Values['DataBase'] := 'DB.fdb';
    FBConnection.Connected := True;
    Application.CreateForm(TfrmApresentacao, frmApresentacao);
    frmApresentacao.makeShowModal();
    cdsProdutos.Open();
    cdsProdutos.First;
    AtualizarBotoes();
  except
    On E: Exception do
    begin
      raise Exception.Create('TfrmTabela.FormShow : ' + E.Message);
    end;
  end;
end;

end.
