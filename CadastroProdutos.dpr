program CadastroProdutos;

uses
  Vcl.Forms,
  FormTabela in 'FormTabela.pas' {frmTabela},
  FormApresentacao in 'FormApresentacao.pas' {frmApresentacao},
  FormRegistro in 'FormRegistro.pas' {frmRegistro};

{$R *.res}

var
  frmTabela: TfrmTabela;

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmTabela, frmTabela);
  Application.Run;
end.
